
    'use strict';
  
    // Creating the module and factory we referenced in the beforeEach blocks in our test file
    angular.module('api.users', [])
    .factory('Users', function() {
        var Users = {};

        var userList = [
            {
              id: '1',
              name: 'Jane',
              role: 'Designer',
              location: 'New York',
              twitter: 'gijane'
            },
            {
              id: '2',
              name: 'Bob',
              role: 'Developer',
              location: 'New York',
              twitter: 'billybob'
            },{
              id: '3',
              name: 'Victor',
              role: 'Developer',
              location: 'Guatemala',
              twitter: 'tuxtor'
            }
          ];
  
        // Retorna todos los usuarios
        Users.all = function() {
            return userList;    
        };

        // Busqueda por Id
        Users.findById = function(id) {
            return userList.find(function(user) {
                return user.id === id;
            });
        };
  
      return Users;
    });