describe('Users factory', function() {
    var Users;

    var expectedUsers = [
        {
          id: '1',
          name: 'Jane',
          role: 'Designer',
          location: 'New York',
          twitter: 'gijane'
        },
        {
          id: '2',
          name: 'Bob',
          role: 'Developer',
          location: 'New York',
          twitter: 'billybob'
        }
      ];

      var expectedSingleUser = {
        id: '2',
        name: 'Bob',
        role: 'Developer',
        location: 'New York',
        twitter: 'billybob'
      };
    
      // Cargamos el modulo via angular mock antes de cada test
      beforeEach(angular.mock.module('api.users'));
    
      // Inyectamos instancia Users antes de cada tests
      beforeEach(inject(function(_Users_) {
        Users = _Users_;
      }));
    
      // Verificamos la existencia de Users
      it('should exist', function() {
        expect(Users).toBeDefined();
      });

      
    // Test sobre Users.all()
    describe('.all()', function() {
        // El metodo existe
        it('should exist', function() {
            expect(Users.all).toBeDefined();
        });

        // Retornan los indicados
        it('should return a hard-coded list of users', function() {
            expect(Users.all()).toEqual(expectedUsers);
        });
    });

    // Test by id
    describe('.findById()', function() {
        // Verificamos la existencia de gind by id
        it('should exist', function() {
            expect(Users.findById).toBeDefined();
        });

        // Verificamos exito con id 2
        it('should return one user object if it exists', function() {
            expect(Users.findById('2')).toEqual(expectedSingleUser);
        });

        // Verificamos fallo con abc
        it('should return undefined if the user cannot be found', function() {
            expect(Users.findById('ABC')).not.toBeDefined();
        });
    });
});