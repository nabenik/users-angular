describe('UsersController', function() {
    var $controller, UsersController, UsersFactory;


     // Lista de usuarios a ser mock
    var mockUserList = [
      { id: '1', name: 'Jane', role: 'Designer', location: 'New York', twitter: 'gijane' },
      { id: '2', name: 'Bob', role: 'Developer', location: 'New York', twitter: 'billybob' },
      { id: '3', name: 'Jim', role: 'Developer', location: 'Chicago', twitter: 'jimbo' },
      { id: '4', name: 'Bill', role: 'Designer', location: 'LA', twitter: 'dabill' }
    ];
  
    // Carga de modulos antes de cada test
    beforeEach(angular.mock.module('ui.router'));
    beforeEach(angular.mock.module('components.users'));
    beforeEach(angular.mock.module('api.users'));
  
    // Instancia de UsersController
    beforeEach(inject(function(_$controller_) {
      $controller = _$controller_;
      UsersController = $controller('UsersController', {});
    }));

    // Instancia de UsersFactory con spy sobre all (para ser un mock)
    beforeEach(inject(function(_$controller_, _Users_) {
      $controller = _$controller_;
      UsersFactory = _Users_;
  
      // Spy sobre all
      spyOn(UsersFactory, 'all').and.callFake(function() {
        return mockUserList;
      });
  
      // Agregamos Factory como dependencia de controlador
      UsersController = $controller('UsersController', { Users: UsersFactory });
    }));
  
    // Verificamos que exista el controlador
    it('should be defined', function() {
      expect(UsersController).toBeDefined();
    });

    // Agregamos expects sobre all y users
    it('should initialize with a call to Users.all()', function() {
      expect(UsersFactory.all).toHaveBeenCalled();
      expect(UsersController.users).toEqual(mockUserList);
    });
  });